package com.sentifi.stockprice.controller;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.time.LocalDate;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.sentifi.stockprice.exception.InvalidDateException;
import com.sentifi.stockprice.model.Price;
import com.sentifi.stockprice.model.TwoHundredsDma;
import com.sentifi.stockprice.service.StockPriceServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StockPriceRestControllerTest {

	private String ticker = "GE";

	@Autowired
	private MockMvc mvc;

	@MockBean
	private StockPriceServiceImpl stockPriceService;

	@Before
	public void setUp() throws Exception {}

	@Test
	public void getClosePrices_WrongDateFormat() throws Exception {
		mvc.perform(get("/api/v2/{tickerSymbol}/closePrice?startDate=abc&endDate=xtz", ticker)
		        .accept(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isNotFound())
		        .andExpect(jsonPath("$.error").value("Not Found"));
	}

	@Test
	public void getClosePrices_EndDateBeforeStartDate() throws Exception {
		given(stockPriceService.getClosePrices(ticker, LocalDate.of(1999, 03, 31), LocalDate.of(1999, 03, 01))).
		willThrow(new InvalidDateException("Date range is invalid"));
		mvc.perform(get("/api/v2/{tickerSymbol}/closePrice", ticker).param("startDate", "1999-03-31")
		        .param("endDate", "1999-03-01").accept(MediaType.APPLICATION_JSON)).andDo(print())
		        .andExpect(jsonPath("$.message").value(containsString("Date range is invalid")));
	}

	@Test
	public void getClosePrices_InternalServerError() throws Exception {
		LocalDate startDate = LocalDate.of(1999, 3, 1);
		LocalDate endDate = LocalDate.of(1999, 3, 31);

		given(stockPriceService.getClosePrices(ticker, startDate, endDate)).willThrow(new RuntimeException("Test"));

		mvc.perform(get("/api/v2/{tickerSymbol}/closePrice", ticker).param("startDate", "1999-03-01")
		        .param("endDate", "1999-03-31").accept(MediaType.APPLICATION_JSON)).andDo(print())
		        .andExpect(jsonPath("$.error_code").value("500")).andExpect(status().isInternalServerError());

	}

	@Test
	public void getClosePrices_NormaCase() throws Exception {
		Price price = new Price();
		price.setTicker(ticker);
		price.setDateClose(Arrays.asList(new String[] {"1999-3-1", "20"}, new String[] {"1999-3-31", "20"}));

		LocalDate startDate = LocalDate.of(1999, 3, 1);
		LocalDate endDate = LocalDate.of(1999, 3, 31);
		given(stockPriceService.getClosePrices(ticker, startDate, endDate)).willReturn(price);

		mvc.perform(get("/api/v2/{tickerSymbol}/closePrice", ticker).param("startDate", "1999-03-01")
		        .param("endDate", "1999-03-31").accept(MediaType.APPLICATION_JSON)).andDo(print())
		        .andExpect(jsonPath("$.Prices", hasSize(1))).andExpect(jsonPath("$.Prices.[0].Ticker").value(ticker))
		        .andExpect(jsonPath("$.Prices.[0].DateClose", hasSize(2))).andExpect(status().isOk());
	}

	@Test
	public void getTwoHundredsDmaByTicker_StartDateIsEmpty() throws Exception {
		mvc.perform(get("/api/v2/{tickerSymbol}/200dma/", ticker).accept(MediaType.APPLICATION_JSON)).andDo(print())
		        .andExpect(status().isNotFound())
		        .andExpect(jsonPath("$.error_code").value("404"));
	}

	@Test
	public void getTwoHundredsDmaByTicker_StartDateIsWrongFormat() throws Exception {
		mvc.perform(get("/api/v2/{tickerSymbol}/200dma/", ticker).param("startDate", "@@@")
		        .accept(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isNotFound())
		        .andExpect(jsonPath("$.error_code").value("404"));
	}

	@Test
	public void getTwoHundredsDmaByTicker_NormalCase() throws Exception {
		TwoHundredsDma dma = new TwoHundredsDma(ticker);
		dma.setAverage(20.312);
		given(stockPriceService.getTwoHundredsDma(ticker, LocalDate.of(1999, 03, 01))).willReturn(dma);
		mvc.perform(get("/api/v2/{tickerSymbol}/200dma/", ticker).param("startDate", "1999-03-01")
		        .accept(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isOk())
		        .andExpect(jsonPath("$.200dma.Ticker").value(ticker)).andExpect(jsonPath("$.200dma.Avg").value(20.31));
	}
	
	
	@Test
	public void getTwoHundredsDmas_InvalidDate() throws Exception {

		mvc.perform(get("/api/v2/200dma/").param("startDate", "XXX").param("tickers", "GE,FE")
		        .accept(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isNotFound())
		        .andExpect(jsonPath("$.message").value(containsString("wrong date format")));
	}
	
	@Test
	public void getTwoHundredsDmas_tickersAreMissing() throws Exception {

		mvc.perform(get("/api/v2/200dma/").param("startDate", "1999-03-31")
		        .accept(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isNotFound())
		        .andExpect(jsonPath("$.message").value(containsString("Ticker symbols are missing")));
	}
	
	
	@Test
	public void getTwoHundredsDmas_ReachMaxTickersNumber() throws Exception {

		mvc.perform(get("/api/v2/200dma/").param("startDate", "1999-03-31").param("tickers", generateTickers())
		        .accept(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isNotFound())
		        .andExpect(jsonPath("$.message").value(containsString("The limit is 1,000 ticker symbols for each request")));
	}

	private String generateTickers() {
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < 1001; i++) {
			builder.append(i);
			builder.append(",");
		}
		return builder.toString();
	}
	
	@Test
	public void getTwoHundredsDmas_InternalError() throws Exception {

		given(stockPriceService.getTwoHundredsDma(Arrays.asList(ticker, "FE"), LocalDate.of(1999, 03, 31))).willThrow(new RuntimeException("Test"));
		mvc.perform(get("/api/v2/200dma/").param("startDate", "1999-03-31").param("tickers", "GE,FE")
		        .accept(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isInternalServerError())
		        .andExpect(jsonPath("$.message").value("Test"));
	}

	
	@Test
	public void getTwoHundredsDmas_NormalCase() throws Exception {
		TwoHundredsDma dma = new TwoHundredsDma(ticker);
		dma.setAverage(20.312);

		TwoHundredsDma dma1 = new TwoHundredsDma("FE");
		dma1.setAverage(1001.0);

		given(stockPriceService.getTwoHundredsDma(Arrays.asList(ticker, "FE"), LocalDate.of(1999, 03, 31))).willReturn(Arrays.asList(dma, dma1));
		mvc.perform(get("/api/v2/200dma/").param("startDate", "1999-03-31").param("tickers", "GE,FE")
		        .accept(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isOk())
		        .andExpect(jsonPath("$.200dma", hasSize(2)));
	}

}
