package com.sentifi.stockprice.service;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.support.membermodification.MemberMatcher.field;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.env.Environment;
import com.sentifi.stockprice.config.StockPriceServiceConfiguration;
import com.sentifi.stockprice.constants.Constants;
import com.sentifi.stockprice.constants.QuandlConstant;
import com.sentifi.stockprice.model.Price;
import com.sentifi.stockprice.model.TwoHundredsDma;
import com.sentifi.stockprice.model.dto.quandl.Dataset;
import com.sentifi.stockprice.util.DateUtils;
import com.sentifi.stockprice.exception.InvalidDateException;

@RunWith(PowerMockRunner.class)
@PrepareForTest(StockPriceServiceConfiguration.class)
public class StockPriceServiceImplTest {

	private static final String ticker = "FE";

	@InjectMocks
	private StockPriceServiceImpl stockPriceService;

	@Mock
	private DataSourceService dataSourceService;

	@Mock
	private MessageSource messageSource;

	@Mock
	private ApplicationContext applicationContext;

	@Mock
	private Environment enviroment;

	@Before
	public void setUp() throws IllegalArgumentException, IllegalAccessException {
		mockStatic(StockPriceServiceConfiguration.class);
		when(StockPriceServiceConfiguration.getApplicationContext()).thenReturn(applicationContext);
		when(applicationContext.getEnvironment()).thenReturn(enviroment);
		when(enviroment.getProperty("date.format")).thenReturn("yyyy-MM-dd");
		CompletionService<TwoHundredsDma> completionService =
		        new ExecutorCompletionService<>(Executors.newFixedThreadPool(1));
		field(StockPriceServiceImpl.class, "completionService").set(stockPriceService, completionService);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getClosePrices_IllegalStartDateAndEndDate() {
		stockPriceService.getClosePrices(ticker, null, null);
	}
	
	@Test(expected = InvalidDateException.class)
	public void getClosePrices_StartDateAfterEndDate() {
		stockPriceService.getClosePrices(ticker, LocalDate.of(2019, 01, 31), LocalDate.of(2019, 01, 1));
	}

	@Test
	public void getClosePrices_filterOutOfRangeOfDate() {
		LocalDate startDate = LocalDate.of(2016, Month.AUGUST, 1);
		LocalDate endDate = LocalDate.of(2016, Month.AUGUST, 31);
		String[] closePrice1 = {"2016-08-01", "30.3"};
		String[] closePrice2 = {"2016-08-31", "31.6"};
		String[] closePrice3 = {"2016-08-10", "33.1"};
		String[] closePrice4 = {"2016-10-01", "28.1"};
		String[] closePrice5 = {"2016-07-29", "36.1"};

		List<String[]> closePrices = Arrays.asList(closePrice1, closePrice2, closePrice3, closePrice4, closePrice5);
		Dataset dataset = new Dataset();
		dataset.setData(closePrices);
		dataset.setDatasetCode(ticker);

		when(dataSourceService.getClosePrices(ticker)).thenReturn(dataset);
		Price actual = stockPriceService.getClosePrices(ticker, startDate, endDate);
		Collection<String[]> dateClose = actual.getDateClose();

		assertThat(actual.getTicker(), is(ticker));
		assertThat(dateClose.size(), is(3));
		assertThat(dateClose, hasItem(closePrice1));
		assertThat(dateClose, hasItem(closePrice2));
		assertThat(dateClose, hasItem(closePrice3));
	}

	@Test(expected = IllegalArgumentException.class)
	public void getTwoHundredsDma_IllegalStartDate() {
		stockPriceService.getTwoHundredsDma(ticker, null);
	}

	@Test
	public void getTwoHundredsDma_NormalCase() {
		LocalDate startDate = LocalDate.of(2015, Month.AUGUST, 10);

		List<String[]> closePrices = generateClosePricesWithDateRange(startDate, Constants.MOVING_NUMBER_200DMA);

		Dataset dataset = new Dataset();
		dataset.setData(closePrices);
		dataset.setDatasetCode(ticker);

		when(dataSourceService.getClosePrices(ticker)).thenReturn(dataset);
		TwoHundredsDma twoHundredsDma = stockPriceService.getTwoHundredsDma(ticker, startDate);

		assertThat(twoHundredsDma, notNullValue());
		assertThat(twoHundredsDma.getTicker(), is(ticker));
		assertThat(twoHundredsDma.getAverage(), is(30.3));
	}

	private List<String[]> generateClosePricesWithDateRange(LocalDate startDate, long days) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(QuandlConstant.DEFAULT_QUANDL_FORMAT);
		return LongStream.rangeClosed(0, days).mapToObj(startDate::plusDays).map(dt -> {
			return new String[] {dt.format(formatter), "30.3"};
		}).collect(Collectors.toList());
	}

	@Test
	public void getTwoHundredsDma_NoData() {
		LocalDate startDate = LocalDate.of(2015, Month.AUGUST, 10);

		when(messageSource.getMessage("error.200dma.no.data", new String[] {DateUtils.formatDate(startDate)},
		        LocaleContextHolder.getLocale())).thenReturn("No data");
		Dataset dataset = new Dataset();
		dataset.setData(Collections.emptyList());
		dataset.setDatasetCode(ticker);

		when(dataSourceService.getClosePrices(ticker)).thenReturn(dataset);
		TwoHundredsDma twoHundredsDma = stockPriceService.getTwoHundredsDma(ticker, startDate);

		assertThat(twoHundredsDma, notNullValue());
		assertThat(twoHundredsDma.getTicker(), is(ticker));
		assertThat(twoHundredsDma.getMessage(), notNullValue());
	}

	@Test(expected = IllegalArgumentException.class)
	public void getTwoHundredsDmas_IllegalStartDate() {
		stockPriceService.getTwoHundredsDma("FE", null);
	}

	@Test
	public void getTwoHundredsDmas_NormalCase() {
		LocalDate startDate = LocalDate.of(2016, Month.AUGUST, 1);
		when(messageSource.getMessage("error.200dma.no.data", new String[] {DateUtils.formatDate(startDate)},
		        LocaleContextHolder.getLocale())).thenReturn("No data");

		when(dataSourceService.getClosePrices(ticker)).thenReturn(generateDataSet("FE"));
		when(dataSourceService.getClosePrices("GE")).thenReturn(generateDataSet("GE"));
		when(dataSourceService.getClosePrices("FB")).thenReturn(generateDataSet("FB"));

		Dataset noDataset = new Dataset();
		noDataset.setDatasetCode("JAV");
		noDataset.setData(Collections.emptyList());

		List<TwoHundredsDma> actual =
		        stockPriceService.getTwoHundredsDma(Arrays.asList(ticker, "JAV", "GE", "FB", "FB", "FB", "FB"), startDate);

		assertThat(actual, notNullValue());
		//check remove duplicate ticker
		assertThat(actual.size(), is(4));
		Map<String, TwoHundredsDma> actualMap =
		        actual.stream().collect(Collectors.toMap(dma -> dma.getTicker(), dma -> dma));

		TwoHundredsDma noData200Dma = actualMap.get("JAV");
		assertThat(noData200Dma, notNullValue());
		assertThat(noData200Dma.getTicker(), is("JAV"));
		assertThat(noData200Dma.getMessage(), notNullValue());
		assertThat(noData200Dma.getPossibleStartDate(), notNullValue());


		TwoHundredsDma dma = actualMap.get(ticker);
		assertThat(dma, notNullValue());
		assertThat(dma.getTicker(), is(ticker));
		assertThat(dma.getAverage(), notNullValue());
	}



	private Dataset generateDataSet(String ticker) {
		String[] closePrice1 = {"2016-08-01", "30.3"};
		String[] closePrice2 = {"2016-08-31", "31.6"};
		String[] closePrice3 = {"2016-08-10", "33.1"};
		String[] closePrice4 = {"2016-10-01", "28.1"};
		String[] closePrice5 = {"2016-07-29", "36.1"};

		List<String[]> closePrices = Arrays.asList(closePrice1, closePrice2, closePrice3, closePrice4, closePrice5);
		Dataset dataset = new Dataset();
		dataset.setData(closePrices);
		dataset.setDatasetCode(ticker);
		return dataset;
	}


}
