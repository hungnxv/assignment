package com.sentifi.stockprice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import com.sentifi.stockprice.exception.TickerNotFoundException;
import com.sentifi.stockprice.model.dto.quandl.ClosePriceResult;
import com.sentifi.stockprice.model.dto.quandl.Dataset;

@Service("dataSourceService")
public class DataSourceServiceQuandlImpl implements DataSourceService {

	private static final Logger logger = LoggerFactory.getLogger(DataSourceServiceQuandlImpl.class);

	private final RetryTemplate retryTemplate;

	@Autowired
	private final RestTemplate restTemplate;

	@Value("${quandl.api.url}")
	private String quandlAPIUrl;

	private final MessageSource messageSource;

	@Autowired
	public DataSourceServiceQuandlImpl(RetryTemplate retryTemplate, RestTemplate restTemplate, MessageSource messageSource) {
		this.retryTemplate = retryTemplate;
		this.restTemplate = restTemplate;
		this.messageSource = messageSource;
	}

	/*
	 * (non-Javadoc)
	 * @see com.sentifi.stockprice.service.DataSourceService#getClosePrices(java.lang.String)
	 */
	@Cacheable(value = "CLOSE_PRICES")
	public Dataset getClosePrices(String ticker) {
		logger.info("Retrieving ClosePrices for ticker {}", ticker);
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			final HttpEntity<String> requestEntity = new HttpEntity<>(headers);

			ClosePriceResult closePriceResult = retryTemplate.execute(r -> {
				String finalUrl = String.format(quandlAPIUrl, ticker);
				logger.info("Sending Request to {}", finalUrl);
				return restTemplate
						.exchange(finalUrl, HttpMethod.GET, requestEntity, ClosePriceResult.class)
						.getBody();
			});
			return closePriceResult.getDataset();

		} catch (HttpClientErrorException e) {
			logger.error("getClosePrices {}", ticker, e);
			throw new TickerNotFoundException(messageSource.getMessage("error.ticker.not.found", new String[] {ticker}, LocaleContextHolder.getLocale()));
		}

	}


}
