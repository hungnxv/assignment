package com.sentifi.stockprice.service;

import java.time.LocalDate;
import java.util.List;
import com.sentifi.stockprice.model.Price;
import com.sentifi.stockprice.model.TwoHundredsDma;

public interface StockPriceService {

	/**
	 * Get The Close Prices for a range of dates
	 * 
	 * @param startDate from date
	 * @param endDate to end
	 * @param ticker ticker symbol
	 * @return Prices contains sorted ClosePrices of the input ticker.
	 * 
	 */
	Price getClosePrices(String ticker, LocalDate startDate, LocalDate endDate);

	/**
	 * get 200DMA for the ticker beginning with a start date. <br>
	 * 200DMA: is a popular technical indicator which investors use to analyze price trends. It is
	 * simply a security's average closing price over the last 200 days.
	 * 
	 * @param ticker
	 * @param startDate The beginning date to calculate the average.
	 * @return Return a 200DMA. If there is no data for the start date, the first possible start date
	 *         will be shown
	 */
	TwoHundredsDma getTwoHundredsDma(String ticker, LocalDate startDate);

	/**
	 * get 200DMA for list of ticker
	 * 
	 * @param tickers list of tickers
	 * @param startDate The beginning date to calculate the average
	 * @return a list of 200DMA
	 */
	List<TwoHundredsDma> getTwoHundredsDma(List<String> tickers, LocalDate startDate);

}
