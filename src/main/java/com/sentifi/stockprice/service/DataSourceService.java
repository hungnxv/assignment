package com.sentifi.stockprice.service;

import com.sentifi.stockprice.model.dto.quandl.Dataset;

public interface DataSourceService {


	/**
	 * Get Close Prices of the ticker. The result will cached for the 10,000 ticker latest. 
	 * By using ticker as key, I get all close prices data that helping reduce the call Quandl API, a service charges money
	 * Quandl API supports getting close price by ticker and date.   
	 * @param ticker
	 * @return list of sorted ClosePrice by Date ascending of the input ticker.
	 * 
	 */
	Dataset getClosePrices(String ticker);
}
