package com.sentifi.stockprice.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import com.sentifi.stockprice.constants.Constants;
import com.sentifi.stockprice.constants.QuandlConstant;
import com.sentifi.stockprice.exception.InvalidDateException;
import com.sentifi.stockprice.model.Price;
import com.sentifi.stockprice.model.TwoHundredsDma;
import com.sentifi.stockprice.model.dto.quandl.Dataset;
import com.sentifi.stockprice.util.DateUtils;

import static java.util.concurrent.CompletableFuture.supplyAsync;

@Service
public class StockPriceServiceImpl implements StockPriceService {

	private static final Logger logger = LoggerFactory.getLogger(StockPriceServiceImpl.class);

	private final DataSourceService dataSourceService;

	private final MessageSource messageSource;

	@Autowired
	public StockPriceServiceImpl(DataSourceService dataSourceService, MessageSource messageSource) {
		this.dataSourceService = dataSourceService;
		this.messageSource = messageSource;
	}


	@Override
	public Price getClosePrices(String ticker, LocalDate startDate, LocalDate endDate) {
		if (!ObjectUtils.allNotNull(ticker, startDate, endDate)) {
			throw new IllegalArgumentException("Ticker, startDate or endDate must not be null");
		}
		
		if (startDate.isAfter(endDate)) {
			throw new InvalidDateException(messageSource.getMessage("error.date.range",
					new String[] {DateUtils.formatDate(startDate), DateUtils.formatDate(endDate)},
					LocaleContextHolder.getLocale()));
		}
		Dataset dataset = dataSourceService.getClosePrices(ticker);
		return new Price(dataset.getDatasetCode(), filterClosePricesByDateRange(dataset.getData(), startDate, endDate));
	}

	private static List<String[]> filterClosePricesByDateRange(List<String[]> datas, LocalDate startDate,
	        LocalDate endDate) {
		return datas.stream().filter(data -> {
			LocalDate date = DateUtils.parseDate(data[QuandlConstant.DATE_COLUMN_INDEX]);
			return !date.isBefore(startDate) && !date.isAfter(endDate);
		}).collect(Collectors.toList());
	}


	@Override
	public TwoHundredsDma getTwoHundredsDma(String ticker, LocalDate startDate) {
		if (!ObjectUtils.allNotNull(ticker, startDate)) {
			throw new IllegalArgumentException("Ticker, startDate must not be null");
		}
		TwoHundredsDma twoHundredsDma = new TwoHundredsDma();
		twoHundredsDma.setTicker(ticker);

		try {
			Dataset dataset = dataSourceService.getClosePrices(ticker);
			LocalDate endDate = startDate.plusDays(Constants.MOVING_NUMBER_200DMA);
			twoHundredsDma.setAverage(calculateAverageTwoHundredsDma(
			        filterClosePricesByDateRange(dataset.getData(), startDate, endDate)));
		} catch (RuntimeException e) {
			logger.error("getTwoHundredsDma", e);
			String possibleStartDate = DateUtils.formatDate(startDate);
			twoHundredsDma.setMessage( messageSource.getMessage("error.200dma.no.data",
			        new String[] {possibleStartDate}, LocaleContextHolder.getLocale()));
			twoHundredsDma.setPossibleStartDate(possibleStartDate);
		}

		return twoHundredsDma;
	}

	private static double calculateAverageTwoHundredsDma(List<String[]> closePrices) {

		if (CollectionUtils.isEmpty(closePrices)) {
			throw new IllegalArgumentException("No data for close prices");
		}

		return closePrices.stream()
		        .mapToDouble(closePrice -> Double.parseDouble(closePrice[QuandlConstant.CLOSE_PRICE_COLUMN_INDEX]))
		        .average().getAsDouble();
	}


	@Override
	public List<TwoHundredsDma> getTwoHundredsDma(List<String> tickerSymbols, LocalDate startDate) {
		if (!ObjectUtils.allNotNull(tickerSymbols, startDate)) {
			throw new IllegalArgumentException("tickerSymbols, startDate must not be null");
		}
		Set<String> noDuplicateTickerSymbols = new HashSet<>(tickerSymbols);

		List<CompletableFuture<TwoHundredsDma>> tasks = new ArrayList<>(noDuplicateTickerSymbols.size());
		noDuplicateTickerSymbols.forEach(ticker ->  tasks.add(supplyAsync(() -> getTwoHundredsDma(ticker, startDate))));

		List<TwoHundredsDma> result = new ArrayList<>(noDuplicateTickerSymbols.size());

		for (CompletableFuture<TwoHundredsDma> task : tasks) {
			try {
				result.add(task.get());
			} catch (InterruptedException e) {
				logger.error("getTwoHundredsDma was interrupted", e);
				Thread.currentThread().interrupt();
			} catch (ExecutionException e) {
				logger.error("getTwoHundredsDma ", e);
			}
		}
		return result;

	}


}
