package com.sentifi.stockprice.controller;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.sentifi.stockprice.exception.RestException;
import com.sentifi.stockprice.model.Price;
import com.sentifi.stockprice.model.TwoHundredsDma;
import com.sentifi.stockprice.service.StockPriceService;
import com.sentifi.stockprice.util.DateUtils;

@RestController
@RequestMapping(value="/api/v2/")
public class StockPriceRestController {

	private static final Logger logger = LoggerFactory.getLogger(StockPriceRestController.class);

	private static final String PRICES = "Prices";
	private static final String NAME_200DMA_RESPONSE = "200dma";
	private static final int MAX_TICKERS_NUMBER = 1000;

	private final StockPriceService stockPriceService;

	private final MessageSource messageSource;

	@Autowired
	public StockPriceRestController(StockPriceService stockPriceService, MessageSource messageSource) {
		this.stockPriceService = stockPriceService;
		this.messageSource = messageSource;
	}

	/**
	 * get close prices rest api
	 * @param tickerSymbol ticker
	 * @return json contains list of close prices 
	 */
	@RequestMapping(value = "{tickerSymbol}/closePrice")
	public ResponseEntity<Map<String, List<Price>>> getClosePrices(
			@PathVariable("tickerSymbol") String tickerSymbol, @RequestParam(value = "startDate") String startDate, @RequestParam("endDate") String endDate) {


		logger.info("getClosePrices (tickerSymbol= {}, startDate ={}, endDate = {}", tickerSymbol, startDate,
		        endDate);

		Map<String, List<Price>> response = new HashMap<>(1);

		Price price = stockPriceService.getClosePrices(tickerSymbol, DateUtils.parseDate(startDate), DateUtils.parseDate(endDate));
		response.put(PRICES, Arrays.asList(price));
		return new ResponseEntity<>(response, HttpStatus.OK);
	}


	/**
	 * get 200 dma by ticker rest api
	 * @param tickerSymbol ticker
	 * @param startDateText
	 * @return json of 200 dma for input ticker
	 */
	@RequestMapping(value = "{tickersymbol}/200dma")
	public ResponseEntity<Object> getTwoHundredsDmaByTicker(@PathVariable("tickersymbol") String tickerSymbol, @RequestParam(value = "startDate") String startDateText) {


		logger.info("getTwoHundredsDmaOfTicker (tickerSymbol= {}, startDate ={}", tickerSymbol, startDateText);
		

		HashMap<Object, Object> response = new HashMap<>(1);

		LocalDate startDate = DateUtils.parseDate(startDateText);
		TwoHundredsDma twoHundredsDma = stockPriceService.getTwoHundredsDma(tickerSymbol, startDate);
		response.put(NAME_200DMA_RESPONSE, twoHundredsDma);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * get list of 200 DMAs by a list of input tickers 
	 * @param tickers
	 * @param startDateText
	 * @return json contains list of 200dmas 
	 */
	@RequestMapping(value = "200dma")
	public ResponseEntity<Map<String, List<TwoHundredsDma>>> getTwoHundredsDmas(@RequestParam(value = "tickers") List<String> tickers, @RequestParam(value = "startDate") String startDateText) {


		logger.info("getTwoHundredsDmas (tickerSymbol= {}, startDate ={}", tickers, startDateText);

		if (tickers.size() > MAX_TICKERS_NUMBER) {
			logger.info("Reach the limit of ticker symbols, sise: {}", tickers.size());
			throw new RestException(messageSource.getMessage("error.request.ticker",
					new Integer[] {tickers.size()},LocaleContextHolder.getLocale()), HttpStatus.NOT_FOUND);
		}

		Map<String, List<TwoHundredsDma>> response = new HashMap<>(1);
		LocalDate startDate = DateUtils.parseDate(startDateText);

		List<TwoHundredsDma> twoHundredsDma =
		        stockPriceService.getTwoHundredsDma(tickers, startDate);
		response.put(NAME_200DMA_RESPONSE, twoHundredsDma);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
