package com.sentifi.stockprice.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import com.sentifi.stockprice.exception.InvalidDateException;
import com.sentifi.stockprice.exception.RestException;
import com.sentifi.stockprice.exception.TickerNotFoundException;
import com.sentifi.stockprice.model.ApiError;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ExceptionHandlingController extends ResponseEntityExceptionHandler {

	@Value("${date.format}")
	private String dateFormat;


	@ExceptionHandler(RestException.class)
	public ResponseEntity<ApiError> processRestException(HttpServletRequest req,
	        RestException ex) {
		ApiError apiError = new ApiError(ex.getMessage(), ex.getErrorCode());
		return new ResponseEntity<>(apiError, ex.getErrorCode());
	}

	@ExceptionHandler(InvalidDateException.class)
	public ResponseEntity<ApiError> processInvalidDateException(HttpServletRequest req,
			InvalidDateException ex) {
		ApiError apiError = new ApiError(ex.getDateErrorMessage(), HttpStatus.NOT_FOUND);
		return new ResponseEntity<>(apiError, apiError.getHttpStatus());
	}

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<ApiError> processException(HttpServletRequest req,
			RuntimeException ex) {
		ApiError apiError = new ApiError(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		return new ResponseEntity<>(apiError, apiError.getHttpStatus());
	}
	
	
	@ExceptionHandler(TickerNotFoundException.class)
	public ResponseEntity<ApiError> processTickerNotFoundException(HttpServletRequest req,
			RuntimeException ex) {
		ApiError apiError = new ApiError(ex.getMessage(), HttpStatus.NOT_FOUND);
		return new ResponseEntity<>(apiError, apiError.getHttpStatus());
	}

}
