package com.sentifi.stockprice.constants;

public final class QuandlConstant {

	private QuandlConstant() {
		throw new AssertionError("Could not be created");
	}

	/**
	 * index of the date in the CLOSEPRICE response data [date, close price]
	 */
	public static final int DATE_COLUMN_INDEX = 0;

	/**
	 * index of the Close price in the response data CLOSEPRICE [date, close price]
	 */
	public static final int CLOSE_PRICE_COLUMN_INDEX = 1;

	/**
	 * defaul date format of Quandl response data
	 */
	public static final String DEFAULT_QUANDL_FORMAT = "yyyy-MM-dd";

}
