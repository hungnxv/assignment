package com.sentifi.stockprice.constants;

public final class Constants {
	
	private Constants () {
		throw new AssertionError("Could not be created");

	}
	/**
	 * 200 Day Moving Average number
	 */
	public static final int MOVING_NUMBER_200DMA = 200;
}
