package com.sentifi.stockprice.config;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Configuration
public class StockPriceServiceConfiguration implements ApplicationContextAware {

	@Value("${quandl.api.connectiontimeout}")
	private long quandlApiConnectionTimeout;

	@Value("${quandl.api.max.attempts}")
	private int quandlApiMaxAttempts;

	private static ApplicationContext applicationContext;

	@Bean
	public RetryTemplate retryTemplate() {
		RetryTemplate retryTemplate = new RetryTemplate();

		ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
		backOffPolicy.setInitialInterval(quandlApiConnectionTimeout);
		retryTemplate.setBackOffPolicy(backOffPolicy);

		Map<Class<? extends Throwable>, Boolean> retryableExceptions = new HashMap<>();
		retryableExceptions.put(HttpClientErrorException.class, Boolean.FALSE);
		retryableExceptions.put(Exception.class, Boolean.TRUE);
		SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy(quandlApiMaxAttempts, retryableExceptions);

		retryTemplate.setRetryPolicy(retryPolicy);
		return retryTemplate;
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Override
	public synchronized void setApplicationContext(ApplicationContext ctx) throws BeansException {
		applicationContext = ctx;
	}

	public static synchronized ApplicationContext getApplicationContext() {
		return applicationContext;
	}


}
