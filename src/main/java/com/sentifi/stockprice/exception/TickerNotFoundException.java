package com.sentifi.stockprice.exception;

public class TickerNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -7733250101107210179L;

	public TickerNotFoundException(String message) {
		super(message);
	}

}
