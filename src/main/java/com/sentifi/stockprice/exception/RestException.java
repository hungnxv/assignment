package com.sentifi.stockprice.exception;

import org.springframework.http.HttpStatus;

public class RestException extends RuntimeException {

	private static final long serialVersionUID = -624897215734677169L;

	private final HttpStatus errorCode;

	
	public RestException(String message, HttpStatus errorCode) {
		super(message);
		this.errorCode = errorCode;
	}


	public HttpStatus getErrorCode() {
		return errorCode;
	}

}
