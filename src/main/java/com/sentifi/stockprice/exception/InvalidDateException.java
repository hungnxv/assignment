package com.sentifi.stockprice.exception;

public class InvalidDateException extends RuntimeException {
	
	private static final long serialVersionUID = 4866117594090390773L;
	
	private final String dateErrorMessage;

	public InvalidDateException(String dateText) {
		super();
		this.dateErrorMessage = dateText;
	}

	public String getDateErrorMessage() {
		return dateErrorMessage;
	}

}
