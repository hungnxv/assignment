package com.sentifi.stockprice.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import com.sentifi.stockprice.config.StockPriceServiceConfiguration;
import com.sentifi.stockprice.constants.QuandlConstant;
import com.sentifi.stockprice.exception.InvalidDateException;

/*
 * 
 * this class only be used after Spring context is started
 */
public final class DateUtils {

	private static final Logger logger = LoggerFactory.getLogger(DateUtils.class);

	private DateUtils() {
		throw new AssertionError("Could not be created");
	}

	//Safe Initialization
	private static class FormatterHolder {
		//formatter is thread safe
		private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(
				StockPriceServiceConfiguration.getApplicationContext().getEnvironment().getProperty("date.format"));
		private static final DateTimeFormatter quandlFormatter = DateTimeFormatter.ofPattern(QuandlConstant.DEFAULT_QUANDL_FORMAT);
	}

	public static LocalDate parseDate(String dateText) {
		try {
			return LocalDate.parse(dateText, FormatterHolder.formatter);
		} catch (DateTimeParseException e) {
			logger.error("DateUtil.parseDate ", e);
			ApplicationContext applicationContext = StockPriceServiceConfiguration.getApplicationContext();
			throw new InvalidDateException(applicationContext
					.getBean(MessageSource.class).getMessage("error.date.invalid",
							new String[] {dateText, applicationContext.getEnvironment().getProperty("date.format")}, LocaleContextHolder.getLocale()));
		}
	}

	public static String formatDate(LocalDate date) {
		return FormatterHolder.formatter.format(date);
	}


	public static String formatQuandDateParam(LocalDate date) {
		return formatDate(date, FormatterHolder.quandlFormatter);
	}

	public static String formatDate(LocalDate date, DateTimeFormatter formatter) {
		if(formatter == null) {
			throw new IllegalArgumentException("Formatter must not be null");
		}
		return formatter.format(date);
	}


}
