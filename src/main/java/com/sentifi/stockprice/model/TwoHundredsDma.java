package com.sentifi.stockprice.model;

import java.io.IOException;
import java.util.Locale;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TwoHundredsDma {
    
    @JsonProperty("Ticker")
    private String ticker;

    @JsonProperty("Avg")
    @JsonSerialize(using = DoubleSerializer.class)
    private Double average;
    
    @JsonProperty("ErrorMessage")
    private String message;
    
    @JsonProperty("PossibleStartDate")
    private String possibleStartDate;
    
    
    public TwoHundredsDma() {
	}
    
	public TwoHundredsDma(String ticker) {
		this.ticker = ticker;
	}
	
	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public Double getAverage() {
		return average;
	}

	public void setAverage(Double average) {
		this.average = average;
	}

	public String getPossibleStartDate() {
		return possibleStartDate;
	}

	public void setPossibleStartDate(String possibleStartDate) {
		this.possibleStartDate = possibleStartDate;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "TwoHundredsDma [ticker=" + ticker + ", average=" + average + ", message=" + message
		        + ", possibleStartDate=" + possibleStartDate + "]";
	}
	
	public static class DoubleSerializer extends JsonSerializer<Double> {

		@Override
		public void serialize(Double value, JsonGenerator generator, SerializerProvider serializers)
		        throws IOException, JsonProcessingException {
		    generator.writeNumber(String.format(Locale.US, "%.2f", value));
		}
		
	}

}
