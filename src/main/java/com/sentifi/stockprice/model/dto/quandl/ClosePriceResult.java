package com.sentifi.stockprice.model.dto.quandl;

public class ClosePriceResult {
	private Dataset dataset;

	public Dataset getDataset() {
		return dataset;
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}


	@Override
	public String toString() {
		return "ClosePriceResult [dataSet=" + dataset + "]";
	}



}
