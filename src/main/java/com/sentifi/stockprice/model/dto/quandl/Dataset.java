package com.sentifi.stockprice.model.dto.quandl;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Dataset {

	@JsonProperty("dataset_code")
	private String datasetCode;

	private List<String[]> data;


	public String getDatasetCode() {
		return datasetCode;
	}

	public void setDatasetCode(String datasetCode) {
		this.datasetCode = datasetCode;
	}

	public List<String[]> getData() {
		return data;
	}

	public void setData(List<String[]> data) {
		this.data = data;
	}

}
