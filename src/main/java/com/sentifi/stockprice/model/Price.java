package com.sentifi.stockprice.model;

import java.util.Collection;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Price {

	@JsonProperty("Ticker")
	private String ticker;

	@JsonProperty("DateClose")
	private Collection<String[]> dateClose;

	public Price() {}

	public Price(String ticker, Collection<String[]> dateClose) {
		this.ticker = ticker;
		this.dateClose = dateClose;
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public Collection<String[]> getDateClose() {
		return dateClose;
	}

	public void setDateClose(Collection<String[]> dateClose) {
		this.dateClose = dateClose;
	}

	@Override
	public String toString() {
		return "Price [ticker=" + ticker + ", dateClose=" + dateClose + "]";
	}

}
