package com.sentifi.stockprice.model;

import org.springframework.http.HttpStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiError {

	@JsonProperty("error_code")
	private int errorCode;

	private String error;

	private String message;

	@JsonIgnore
	private HttpStatus httpStatus;

	public ApiError() {}

	public ApiError(String message, HttpStatus status) {
		super();
		this.message = message;
		this.errorCode = status.value();
		this.error = status.getReasonPhrase();
		this.httpStatus = status;
	}



	public int getErrorCode() {
		return errorCode;
	}


	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}


	public String getError() {
		return error;
	}


	public void setError(String error) {
		this.error = error;
	}


	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	@Override
	public String toString() {
		return "ApiError [errorCode=" + errorCode + ", error=" + error + ", message=" + message + "]";
	}

}
