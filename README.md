# stock-price-service

## Prerequisite
- Java 1.8
- Maven

## Build:

- mvn clean install

## Run:

- mvn spring-boot:run
- execute stockPriceService.jar

## APIs:

### 4.1 Get close price of input ticker in specific date range

GET: http://localhost:8080/api/v2/{tickerSymbol}/closePrice?startDate={yyyy-MM-dd}&endDate={yyyy-MM-dd}

Response:

SUCCESS:
```

{
 "Prices":
	[
	 {
	 	"Ticker":"FE",
		"DateClose":[[{yyyy-MM-dd},{double}]
	} ]
}
```

FAILED:
```

{
	"error":"Bad Request",
	"message":"start date {startDate} or end date {endDate} is wrong date format. Please provide date with format 	yyyy-MM-dd",
	"error_code":400
}
```

### 4.2 Calculate 200 days moving average of a ticker and startDate

GET: http://localhost:8080/api/v2/{tickerSymbol}/200dma?startDate={yyyy-MM-dd}

Response

SUCCESS:
```
{
		"200dma":{
			"Ticker":{tickerSymbol},
			"Avg":{double}
		}
}
```

FAILED:

```

{
		"200dma":
			{
				"Ticker": {tickerSymbol},
				"Avg":{double},
				"ErrorMessage":"No data for given date {yyyy-MM-dd}",
				"PossibleStartDate":{yyyy-MM-dd}
			}
}
```

### 4.3 Calculate 200 days moving average of list of tickers and startDate

GET: http://localhost:8080/api/v2/200dma?startDate={yyyy-MM-dd}&tickers={tickers}

param: {tickers} separated by comma. For Example: tickers=FE,GE,DE,FB,ABC

Response:

SUCCESS:

```
{
	"200dma":
		[
			{
				"Ticker":"{tickerSymbol1}",
				"Avg":0.00,
				"ErrorMessage":"No data for given date {yyyy-MM-dd",
				"PossibleStartDate":{yyyy-MM-dd}
			},
			
			{
				"Ticker":"{tickerSymbol2}",
				"Avg":{double}
			}
		]
}
```




